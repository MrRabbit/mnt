# mnt (mount)

Mount configured media (luks of sftp)

This script mounts, unmounts and query mount status of sftp or luks media.

This script requires the media to be specified in its config.

See ```mnt -h``` for help.

## Install

- This script relies on configfile, install this repo in your PYTHONPATH first.
- copy mnt.py (or symlink it without the .py extension to a folder in $PATH)

## Configuration

The config file is ```~/mrrabbit/mnt/config.json``` according to the *configfile* workflow.

Example :

```json
{
    "mount_folder": "~/folder/", 
    "sftp": {
        "name": "remote_ssh:remote_folder",
        "name": ["remote_ssh:remote_folder", "/local/mount/point/"]
    },
    "luks": {
        "luks_name": ["/dev/disk", "mapper_name"],
        "luks_name": ["/dev/disk", "mapper_name", "/local/mount/point/"]
    }
}
```

"mount_folder" is an optional common mount folder, it is only used if a mountpoint is not specified.
If mount point is not specified, "mount_folder/media name" is used. The mount point must already exists.

### sftp

For sftp, the following syntaxes are accepted:

- "media name": "remote path" 
- "media name": \["remote path","/mount/point"] to specify the mount point.

Mounting an sftp folder requires no special privilege if you have access to the mount point.

### luks

For luks, the following syntaxes are accepted:

- "media name": \["/dev/disk", "mapper_name"],
- "media name": \["/dev/disk", "mapper_name", "/mount/point"]

Where "mapper_name" is the name in /dev/mapper/.

## Commands

```mnt -m <name>``` to mount a media
```mnt -u <name>``` to umount a media

Query: True/1 means mounted, False,/0 means unmounted. This is the opposite of the command "mountpoint"

```mnt -q <name>``` print 1 if mounted, 0 otherwise
```mnt -Q <name>``` return 1 if mounted, 0 otherwise

## As a python library 

- list_fs() : return a list of all configured repositories
- mount_fs(name): mount the media
- umount_fs(name): umount the media
- query_mounted(name): get whether the media is mounted or not

## zsh completion

_mnt provides zsh completion to list configured mdia and commands help.
