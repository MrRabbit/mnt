#!/usr/bin/env python

'''
mount luks encrypted disks or sftp directories
'''

import argparse
import os
import sys
from subprocess import call
from configfile import ConfigFile

__version__ = '0.0.1'

cf = ConfigFile('mrrabbit', 'mnt')

# ### Parse config

#common moun point folder
mount_folder=cf.get('mount_folder', default='')

# mount_point: ssh config name
sftp = {}
for name, params in cf.get('sftp').items():
    if type(params) == list:
        sftp[name] = {'remote': params[0]}
        mp = params[1]
    else:
        if not mount_folder:
            print('common mount point used but not defined')
            sys.exit(1)
        sftp[name] = {'remote': params}
        mp = mount_folder + '/' + name
    sftp[name]['mount_point'] = os.path.expanduser(mp)

# luks: mount point/volume group name: (encrypted disk, mapper name)
luks = {}
for name, params in cf.get('luks').items():
    luks[name] = {'disk': params[0], 'mapper': params[1]}
    if len(params) > 2:
        mp = params[2]
    else:
        if not mount_folder:
            print('common mount point used but not defined')
            sys.exit(1)
        mp = mount_folder + '/' + name
    luks[name]['mount_point'] = os.path.expanduser(mp)

# ### Private 

def _run(cmds, exit_on_error = True):
    for i in cmds:
        if args.debug:
            print(' '.join(i))
        ret = call(i)
        if ret != 0 and exit_on_error:
            print('error')
            sys.exit(1)

class sftp_f:
    def mount(self, name, params):
        _run([['sshfs', '-C', params["remote"], params["mount_point"]]])

    def umount(self, name, params):
        _run([['umount', params["mount_point"]]])

class luks_f:
    def mount(self, name, params):
        _run([
            ['sudo', 'cryptsetup', 'open', params["disk"], name],
            ['sudo', 'vgchange', '-ay'],
            ['sudo', 'mount',
                '/dev/mapper/' + params["mapper"],
                params["mount_point"]]
        ])

    def umount(self, name, params):
        _run([
            ['sudo', 'umount', params["mount_point"]],
            ['sudo', 'cryptsetup', 'close', params["mapper"]],
            ['sudo', 'cryptsetup', 'close', name]
        ])

def fs_type(name):
    if name in sftp.keys():
        return (sftp_f(), sftp[name])
    elif name in luks.keys():
        return (luks_f(), luks[name])
    else:
        print('unknown target')
        sys.exit(-1)

# ### Public

def list_fs():
    ''' list configured filesystems '''
    return list(sftp.keys()) + list(luks.keys())

def mount_fs(name):
    hdlr, params = fs_type(name)
    hdlr.mount(name, params)

def umount_fs(name):
    ''' umount a configured filesystem '''
    hdlr, params = fs_type(name)
    hdlr.umount(name, params)

def query_mounted(name):
    ''' return 1 if mounted, 0 otherwise '''
    _, params = fs_type(name)
    return bool(1 - call(["mountpoint", "-q", params["mount_point"]]))

if __name__ == '__main__':

    # Parse arguments, see --help option for more information
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '-v', '--version',
        action='version',
        version='%(prog)s {version}'.format(version=__version__))
    parser.add_argument(
        '-d', '--debug',
        help='debug mode, verbose output',
        action='store_true')
    parser.add_argument(
        '-l', '--list', dest='l',
        help='list all defined mount points',
        action='store_true')
    parser.add_argument('-m', '--mount', help='mount point name')
    parser.add_argument('-u', '--umount', help='umount point name')
    parser.add_argument('-q', '--query', help='print 1 if mounted, 0 otherwise')
    parser.add_argument('-Q', '--query_ret', help='return 1 if mounted, 0 otherwise')
    args = parser.parse_args()

    if args.l:
        for i in list_fs():
            print(i)
    elif args.mount:
        mount_fs(args.mount)
    elif args.umount:
        umount_fs(args.umount)
    elif args.query:
        print(query_mounted(args.query))
    elif args.query_ret:
        sys.exit(query_mounted(args.query_ret))
    else:
        parser.print_help(sys.stderr)
        sys.exit(1)
